\context Staff = "tenor" \with { \consists Ambitus_engraver } <<
	\set Staff.instrumentName = "Tenor"
	\set Staff.shortInstrumentName = "T."
	\set Staff.midiInstrument = "Voice Oohs"
	\set Score.skipBars = ##t
	\set Staff.printKeyCancellation = ##f
	\new Voice \global
	\new Voice \globalTempo

	\context Voice = "voz-tenor" {
		\override Voice.TextScript #'padding = #2.0
		\override MultiMeasureRest #'expand-limit = 1

		\time 4/4
		\clef "treble_8"
		\key d \major

		R1*4  |
%% 5
		r4 fis 8 fis 4 e 8 d 4  |
		e 4. a r8 d  |
		d 4 d d 8 b, 4 a, 8 ~  |
		a, 1  |
		r4 fis 8 fis 4 e 8 d 4  |
%% 10
		e 4 a 8 a 4 r8 d d  |
		d 4 d d 8 b, 4 a, 8 ~  |
		a, 1  |
		r4 b, 8 b, 4 cis 8 d 4  |
		a, 4. d r8 d  |
%% 15
		e 4 e fis 8 e 4 d 8 ~  |
		d 1  |
		r4 b, 8 b, 4 cis 8 d 4  |
		a, 4. d d 8 d  |
		d 8 d d d 4 cis 8 d 4  |
%% 20
		e 1 ~  |
		e 2 r4 r8 a,  |
		fis 4 fis g 8 a 4 a 8 ~  |
		a 4. e r8 e  |
		d 4 d d 8 e d fis ~  |
%% 25
		fis 2. r4  |
		b, 8 b, b, b, 4 cis d 8  |
		a, 4. d d 8 d  |
		d 4 d cis 8 d 4 e 8 ~  |
		e 1 ~  |
%% 30
		e 2 r4 r8 d  |
		d' 8 d' d' d' d' d' 4 cis' 8 ~  |
		cis' 4. a 2 a 8  |
		b 8 b b b 4. a 8 a ~  |
		a 2. r8 g  |
%% 35
		g 8 g g g 4 fis 8 e 4  |
		fis 4 a 8 a 4. d 8 d  |
		d 4 fis e d 8 d ~  |
		d 1  |
		R1  |
%% 40
		r4 fis 8 fis 4 e 8 d 4  |
		e 4. a 4 r8 d d  |
		d 4 d 8 d 4. b, 8 b, ~  |
		b, 8 a, 2. r8  |
		r4 fis 8 fis 4 e 8 d 4  |
%% 45
		e 4. a r8 d  |
		d 4 d d 8 b, 4 a, 8 ~  |
		a, 1  |
		r4 b, 8 b, 4 cis 8 d 4  |
		a, 4. d 4 d 8 d d  |
%% 50
		e 4 e fis 8 e 4 e 8 ~  |
		e 8 d 2. r8  |
		r4 b, 8 b, 4 cis 8 d 4  |
		a, 4. d 4 r8 d d  |
		d 8 d d d 4 cis 8 d 4  |
%% 55
		e 1 ~  |
		e 2 r4 r8 a,  |
		fis 4 fis g 8 a 4 a 8 ~  |
		a 4. e r8 e  |
		d 4 d d 8 e d fis ~  |
%% 60
		fis 2. r4  |
		b, 8 b, b, b, 4 cis d 8  |
		a, 4. d d 8 d  |
		d 4 d cis 8 d 4 e 8 ~  |
		e 1 ~  |
%% 65
		e 2 r4 r8 d  |
		d' 8 d' d' d' d' d' 4 cis' 8 ~  |
		cis' 4. a 2 a 8  |
		b 8 b b b 4. a 8 a ~  |
		a 2. r8 g  |
%% 70
		g 8 g g g 4 fis 8 e 4  |
		fis 4 a 8 a 4. d 8 d  |
		d 4 fis e d 8 d ~  |
		d 1  |
		r2 r4 r8 d  |
%% 75
		d' 8 d' d' d' d' d' 4 cis' 8 ~  |
		cis' 4. a 2 a 8  |
		b 8 b b b 4. a 8 a ~  |
		a 2. r8 g  |
		g 8 g g g 4 fis 8 e 4  |
%% 80
		fis 4 a 8 a 4. d 8 d  |
		d 4 fis e d 8 d ~  |
		d 1  |
		R1  |
		\bar "|."
	}

	\new Lyrics \lyricsto "voz-tenor" {
		En tor -- "no a" Ma -- rí -- a y siem -- "pre en" o -- ra -- ción, __
		en un so -- "lo es" -- pí -- ri -- tu "y en" un mis -- mo co -- ra -- zón. __
		En tor -- "no a" Ma -- rí -- a, la Igle -- sia se for -- mó __
		cuan "do es" -- tan -- do jun -- tos, el es -- pí -- ri -- tu se de -- rra -- mó. __

		Ma -- rí -- a, ma -- dre nues -- tra,
		Ma -- rí -- a, ma -- dre de Dios. __
		Ma -- dre de luz "y es" -- pe -- ran -- za,
		por -- ta -- do -- ra del a -- mor. __

		Ma -- rí -- a, ma -- dre de "la I" -- gle -- sia,
		es -- ta -- mos en tor -- "no a" ti __
		u -- ni -- dos en un so -- "lo Es" -- pí -- ri -- tu
		"y en" un mis -- mo co -- ra -- zón. __

		Es -- tan -- do reu -- ni -- dos en el a -- po -- sen -- to al -- to
		un vien -- "to im" -- pe -- tuo -- so lle -- nó to -- "do el" lu -- gar, __
		y len -- guas de fue -- go en ca -- da u -- no se po -- sa -- ron,
		"y en" tor -- "no a" Ma -- rí -- a, el Es -- pí -- ri -- tu se de -- rra -- mó. __

		Ma -- rí -- a, ma -- dre nues -- tra,
		Ma -- rí -- a, ma -- dre de Dios. __
		Ma -- dre de luz "y es" -- pe -- ran -- za,
		por -- ta -- do -- ra del a -- mor. __

		Ma -- rí -- a, ma -- dre de "la I" -- gle -- sia,
		es -- ta -- mos en tor -- "no a" ti __
		u -- ni -- dos en un so -- "lo Es" -- pí -- ri -- tu
		"y en" un mis -- mo co -- ra -- zón. __

		Ma -- rí -- a, ma -- dre de "la I" -- gle -- sia,
		es -- ta -- mos en tor -- "no a" ti __
		u -- ni -- dos en un so -- "lo Es" -- pí -- ri -- tu
		"y en" un mis -- mo co -- ra -- zón. __
	}
>>
