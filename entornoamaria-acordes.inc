\context ChordNames
	\chords {
		\set majorSevenSymbol = \markup { "maj7" }
		\set chordChanges = ##t
		% intro
		d1 a1 g1 d1

		% estrofa
		d1 a1 g1 d1
		d1 a1 g1 d1
		g1 d1 fis1 b1:m
		g1 d1 e1:m a1 a1

		% precoro
		d1 a1 g1 d1
		g1 d1 e1:m a1 a1

		% coro
		b1:m fis1:m g2 a2 d1
		g1 d4. b8:m ~ b2:m e2:m a2 d1
		d1

		% estrofa
		d1 a1 g1 d1
		d1 a1 g1 d1
		g1 d1 fis1 b1:m
		g1 d1 e1:m a1 a1

		% precoro
		d1 a1 g1 d1
		g1 d1 e1:m a1 a1

		% coro
		b1:m fis1:m g2 a2 d1
		g1 d4. b8:m ~ b2:m e2:m a2 d1
		d1

		b1:m fis1:m g2 a2 d1
		g1 d4. b8:m ~ b2:m e2:m a2 d1
		d1
	}
